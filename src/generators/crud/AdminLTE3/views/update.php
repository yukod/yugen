<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();

echo "<?php\n";
?>

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */

$this->title = <?= $generator->generateString('Update {modelClass}: ', ['modelClass' => Inflector::camel2words(StringHelper::basename($generator->modelClass))]) ?> . ' ' . $model-><?= $generator->getNameAttribute() ?>;
$this->params['breadcrumbs'][] = ['label' => <?= $generator->generateString(Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass)))) ?>, 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model-><?= $generator->getNameAttribute() ?>, 'url' => ['view', <?= $urlParams ?>, 'id' => $model->id]];
$this->params['breadcrumbs'][] = <?= $generator->generateString('Update') ?>;
?>
<div class="container-fluid <?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-update">

    <div class="card">
        <div class="card-body d-flex p-0">
            <div class="btn-group p-2">
                <?= "<?= " ?>Html::a(<?= $generator->generateString('<i class=\'fa fa-align-justify\'></i> All ' . Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>, ['index'], ['class' => 'btn btn-default']) ?>
                <?= "<?= " ?>Html::a(<?= $generator->generateString('<i class=\'fa fa-plus\'></i> New ' . Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>, ['create'], ['class' => 'btn btn-primary']) ?>
            </div>
            <div class="btn-group p-2">
                <?= "<?= " ?>Html::a(<?= $generator->generateString('<i class=\'fa fa-eye\'></i> View ') ?>, ['view', 'id' => $model->id], ['class' => 'btn btn-default']) ?>
                <?= "<?= " ?>Html::a(<?= $generator->generateString('<i class=\'fa fa-save\'></i> Save ') ?>, [''], ['class' => 'btn btn-default', 'id' => '<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-save']) ?>
                <?= "<?= " ?> Html::a('<i class=\'fa fa-plus-square\'></i> Save &amp; Add New', [''], ['class' => 'btn btn-default', 'id' => 'vendor-save-more']) ?>
                <?= "<?= " ?>Html::a(<?= $generator->generateString('<i class=\'fa fa-trash-o\'></i> Delete ') ?>, ['delete', <?= $urlParams ?>], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => <?= $generator->generateString('Are you sure you want to delete this item?') ?>,
                        'method' => 'post',
                    ],
                ]) ?>
            </div>
        </div>
    </div>

    <?= "<?= " ?>$this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
<?= "<?php\n" ?>
$this->registerJs('$("#<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-save").click(function(e) {
    e.preventDefault();
    $("#save-more").remove();
    $("#<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-form").submit();
});');

// default is to save and add more
$this->registerJs('$("#<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-save-more").click(function(e) {
    e.preventDefault();
    $("#<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-form").submit();
});');
