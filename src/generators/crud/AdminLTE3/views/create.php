<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

echo "<?php\n";
?>

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */

$this->title = <?= $generator->generateString('Create ' . Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>;
$this->params['breadcrumbs'][] = ['label' => <?= $generator->generateString(Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass)))) ?>, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid <?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-create">

    <div class="card">
        <div class="card-body d-flex p-0">
            <div class="btn-group p-2">
                <?= "<?= " ?>Html::a(<?= $generator->generateString('<i class=\'fa fa-align-justify\'></i> All ' . Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>, ['index'], ['class' => 'btn btn-default']) ?>
            </div>
            <div class="btn-group p-2">
                <?= "<?= " ?>Html::a(<?= $generator->generateString('<i class=\'fa fa-save\'></i> Save ') ?>, [''], ['class' => 'btn btn-default', 'id' => '<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-save']) ?>
                <?= "<?= " ?> Html::a('<i class=\'fa fa-plus-square\'></i> Save &amp; Add New', [''], ['class' => 'btn btn-default', 'id' => 'vendor-save-more']) ?>
            </div>
        </div>
    </div>

    <?= "<?= " ?>$this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
<?= "<?php\n" ?>
$this->registerJs('$("#<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-save").click(function(e) {
    e.preventDefault();
    $("#save-more").remove();
    $("#<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-form").submit();
});');

// default is to save and add more
$this->registerJs('$("#<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-save-more").click(function(e) {
    e.preventDefault();
    $("#<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-form").submit();
});');
