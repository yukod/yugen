<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();
$modelClass = StringHelper::basename($generator->modelClass);
$modelSlug = Inflector::slug(Inflector::camel2id($modelClass));

echo "<?php\n";
?>

use yii\bootstrap4\LinkPager;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use <?= $generator->indexWidgetType === 'grid' ? "yii\\grid\\GridView" : "yii\\widgets\\ListView" ?>;
use yukod\yugen\assets\YugenAsset;

/* @var $this yii\web\View */
<?= !empty($generator->searchModelClass) ? "/* @var \$searchModel " . ltrim($generator->searchModelClass, '\\') . " */\n" : '' ?>
/* @var $dataProvider yii\data\ActiveDataProvider */

YugenAsset::register($this);

$this->title = <?= $generator->generateString(Inflector::pluralize(Inflector::camel2words($modelClass))) ?>;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid <?= Inflector::camel2id($modelClass) ?>-index mb-0 pb-4">

<?php if(!empty($generator->searchModelClass)): ?>
<?= "    <?php " . ($generator->indexWidgetType === 'grid' ? "// " : "") ?>echo $this->render('_search', ['model' => $searchModel]); ?>
<?php endif; ?>

    <?= "<?php" ?> if (Yii::$app->session->hasFlash('admin-flash-level')): ?>
        <div class="alert alert-<?= "<?=" ?> Yii::$app->session->getFlash('admin-flash-level') ?> alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?= "<?=" ?> Yii::$app->session->getFlash('admin-flash-message') ?>
        </div>
    <?= "<?php " ?> endif ?>

    <?= "<?php " ?> if (Yii::$app->session->hasFlash('admin-filter-message')): ?>
    <div class="text-muted"><?= Yii::$app->session->getFlash('admin-filter-message') ?></div>
    <?= "<?php " ?> endif ?>

    <?php
    $columns = [];

    if (($tableSchema = $generator->getTableSchema()) === false) {
        $columns = $generator->getColumnNames();
    } else {
        $columns = $tableSchema->columns;
    }
    ?>

    <div class="card card-outline card-info" style="margin-bottom: 0 !important;">
        <div class="card-header">
            <div class="row">
                <div class="col">
                    <form action="" id="search-form">
                        <div class="form-group mb-0">
                            <div class="input-group">
                                <input type="text" name="search_<?= $modelSlug ?>" id="search-<?= $modelSlug ?>" class="form-control search-field" autocomplete="off" placeholder="search <?= Inflector::camel2id($modelClass) ?> ...">
                                <span class="input-group-append">
                                    <button type="button" class="btn btn-default"><i class="fas fa-search"></i></button>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-2">
                    <?= "<?= " ?> Html::a('Add New', ['create'], ['class' => 'btn btn-info d-block']) ?>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <?= "<?php " ?> $dataProvider->getModels(); $pagination = $dataProvider->pagination ?>
                    <div class="text-left text-muted"><small>Showing <strong><?= "<?= " ?> ($pagination->offset + 1) . " - " . ($pagination->offset + $pagination->pageSize) ?> </strong> of <strong><?= "<?= " ?> $pagination->totalCount ?></strong> items.</small></div>
                </div>
            </div>
        </div>
        <div class="card-body table-responsive p-0">

            <?php if ($generator->indexWidgetType === 'grid'): ?>
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>#</th>

<?php $count = 0; ?>
<?php foreach ($columns as $column): ?>
    <?php if ($column->name == 'id' || $count > 4): ?>
        <?= "<!-- " ?><th><?= $column->name ?></th> -->
    <?php else: ?>
        <th><?= $column->name ?></th>
    <?php endif ?>
    <?php $count++ ?>
<?php endforeach ?>

                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?= "<?php" ?> if (count($dataProvider->getModels()) <= 0): ?>
                    <tr class="text-center">
                        <td colspan="6">No data available ...</td>
                    </tr>
                    <?= "<?php" ?> endif ?>

                    <?= "<?php"?> foreach ($dataProvider->getModels() as $i => $model): ?>
                    <tr>
                        <td><?= "<?=" ?> $dataProvider->pagination->offset + $i + 1 ?></td>

<?php $count = 0; ?>
<?php foreach ($columns as $column): ?>
    <?php if ($column->name == 'id' || $count > 4): ?>
        <?= "<!--" ?><td><?= "<?=" ?> Html::encode($model-><?= $column->name ?>) ?></td> -->
    <?php else: ?>
        <td><?= "<?=" ?> Html::encode($model-><?= $column->name ?>) ?></td>
    <?php endif ?>
    <?php $count++ ?>
<?php endforeach ?>

                        <td>
                            <div class="btn-group float-right" role="group" aria-label="Row Actions">
                                <a href="<?= "<?="?> Url::toRoute(['<?= $modelSlug ?>/update', 'id' => $model->id]) ?>" class="btn btn-sm btn-default"><span class="fa fa-edit"></span></a>
                                <a href="<?= "<?="?> Url::toRoute(['<?= $modelSlug ?>/view', 'id' => $model->id]) ?>" class="btn btn-sm btn-default"><span class="fa fa-eye"></span></a>
                                <a href="<?= "<?="?> Url::toRoute(['<?= $modelSlug ?>/delete', 'id' => $model->id]) ?>" class="btn btn-sm btn-danger" data-method="post"><span class="fa fa-trash"></span></a>
                            </div>
                        </td>
                    </tr>
                    <?= "<?php " ?>endforeach ?>
                </tbody>
            </table>
            <?php else: ?>
                <?= "<?= " ?>ListView::widget([
                    'dataProvider' => $dataProvider,
                    'itemOptions' => ['class' => 'item'],
                    'itemView' => function ($model, $key, $index, $widget) {
                        return Html::a(Html::encode($model-><?= $nameAttribute ?>), ['view', <?= $urlParams ?>]);
                    },
                ]) ?>
            <?php endif; ?>

        </div>

        <div class="card-footer">
            <div class="row">
                <div class="col">
                    <?= "<?php " ?>$pagination = $dataProvider->pagination ?>
                    <div class="text-left text-muted"><small>Showing <strong><?= "<?= " ?>($pagination->offset + 1) . " - " . ($pagination->offset + $pagination->pageSize) ?> </strong> of <strong><?= "<?= " ?>$pagination->totalCount ?></strong> items.</small></div>
                </div>
                <div class="col">
                    <?= "<?= " ?>LinkPager::widget([
                        'pagination' => $dataProvider->pagination,
                        'options' => [
                            'class' => ['float-right'],
                        ],
                        'listOptions' => [
                            'class' => ['pagination', 'pagination-sm', 'mb-0']
                        ],
                        'linkContainerOptions' => [
                            'class' => ['page-item', 'mb-0']
                        ]
                    ]); ?>
                </div>
            </div>
        </div>
    </div>

</div>

<?php
$cols = [];
foreach ($columns as $column) {
    $cols[] = $column->name;
}
?>

<?= "<?php" ?>

if (isset($searchModel)) {
    $this->registerJs('var searchModel = "' . (new ReflectionClass($searchModel::className()))->getShortName() . '";', View::POS_HEAD);
    $this->registerJs(sprintf('var columns = %s;', json_encode('<?= implode(',', $cols) ?>')), View::POS_HEAD);
}
