<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

/* @var $model \yii\db\ActiveRecord */
$model = new $generator->modelClass();
$safeAttributes = $model->safeAttributes();
if (empty($safeAttributes)) {
    $safeAttributes = $model->attributes();
}

echo "<?php\n";
?>

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-form">

    <?= "<?php " ?>$form = ActiveForm::begin(['id' => '<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-form']); ?>

        <div class="row">
            <div class="col-md-12">
                <?= "<?php" ?> if (Yii::$app->session->hasFlash('saveSuccess')): ?>
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?= "<?=" ?> Yii::$app->session->getFlash('saveSuccess') ?>
                    </div>
                <?= "<?php" ?>  endif ?>
                <div class="card card-default">
                    <div class="card-body">

<?php foreach ($generator->getColumnNames() as $attribute) {
    if (in_array($attribute, $safeAttributes)) {
        echo "    <?= " . $generator->generateActiveField($attribute) . " ?>\n\n";
    }
} ?>
                        <?= "<?=" ?> Html::hiddenInput('save_more', true, ['id' => 'save-more']) ?>

                        <div class="form-group">
                            <?= "<?=" ?> Html::submitButton('Submit', ['class' => 'btn btn-primary', 'style' => 'border:none; padding:0; font-size:0; tab-index:-1;']) ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    <?= "<?php " ?>ActiveForm::end(); ?>

</div>
