<?php
/**
 * This is the template for generating a CRUD controller class file.
 */

use yii\db\ActiveRecordInterface;
use yii\helpers\StringHelper;


/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$controllerClass = StringHelper::basename($generator->controllerClass);
$modelClass = StringHelper::basename($generator->modelClass);
$searchModelClass = StringHelper::basename($generator->searchModelClass);
if ($modelClass === $searchModelClass) {
    $searchModelAlias = $searchModelClass . 'Search';
}

/* @var $class ActiveRecordInterface */
$class = $generator->modelClass;
$pks = $class::primaryKey();
$urlParams = $generator->generateUrlParams();
$actionParams = $generator->generateActionParams();
$actionParamComments = $generator->generateActionParamComments();

echo "<?php\n";
?>

namespace <?= StringHelper::dirname(ltrim($generator->controllerClass, '\\')) ?>;

use Yii;
use <?= ltrim($generator->modelClass, '\\') ?>;
<?php if (!empty($generator->searchModelClass)): ?>
use <?= ltrim($generator->searchModelClass, '\\') . (isset($searchModelAlias) ? " as $searchModelAlias" : "") ?>;
<?php else: ?>
use yii\data\ActiveDataProvider;
<?php endif; ?>
use <?= ltrim($generator->baseControllerClass, '\\') ?>;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * <?= $controllerClass ?> implements the CRUD actions for <?= $modelClass ?> model.
 */
class <?= $controllerClass ?> extends <?= StringHelper::basename($generator->baseControllerClass) . "\n" ?>
{
    public function behaviors()
    {
        return [
            /*'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'view', 'update', 'delete'],
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'view', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],*/
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all <?= $modelClass ?> models.
     * @return mixed
     */
    public function actionIndex()
    {
        // if (!Yii::$app->user->can('list<?= $modelClass ?>')) {
        //     throw new ForbiddenHttpException("You are not allowed to perform this action.");
        // }

<?php if (!empty($generator->searchModelClass)): ?>
        $searchModel = new <?= isset($searchModelAlias) ? $searchModelAlias : $searchModelClass ?>();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (isset(Yii::$app->request->queryParams['<?= isset($searchModelAlias) ? $searchModelAlias : $searchModelClass ?>'])) {
            if (isset(Yii::$app->request->queryParams['<?= isset($searchModelAlias) ? $searchModelAlias : $searchModelClass ?>']['title'])) {
                Yii::$app->session->setFlash('admin-filter-message', 'Search result for <strong>' . Yii::$app->request->queryParams['<?= isset($searchModelAlias) ? $searchModelAlias : $searchModelClass ?>']['title'] . '</strong> :');
            } else if (isset(Yii::$app->request->queryParams['<?= isset($searchModelAlias) ? $searchModelAlias : $searchModelClass ?>']['name'])) {
                Yii::$app->session->setFlash('admin-filter-message', 'Search result for <strong>' . Yii::$app->request->queryParams['<?= isset($searchModelAlias) ? $searchModelAlias : $searchModelClass ?>']['name'] . '</strong> :');
            }
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
<?php else: ?>
        $dataProvider = new ActiveDataProvider([
            'query' => <?= $modelClass ?>::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
<?php endif; ?>
    }

    /**
     * Displays a single <?= $modelClass ?> model.
     * <?= implode("\n     * ", $actionParamComments) . "\n" ?>
     * @return mixed
     */
    public function actionView(<?= $actionParams ?>)
    {
        // if (!Yii::$app->user->can('view<?= $modelClass ?>')) {
        //     throw new ForbiddenHttpException("You are not allowed to perform this action.");
        // }

        return $this->render('view', [
            'model' => $this->findModel(<?= $actionParams ?>),
        ]);
    }

    /**
     * Creates a new <?= $modelClass ?> model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        // if (!Yii::$app->user->can('create<?= $modelClass ?>')) {
        //     throw new ForbiddenHttpException("You are not allowed to perform this action.");
        // }

        $model = new <?= $modelClass ?>();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('admin-flash-level', 'success');
            Yii::$app->session->setFlash('admin-flash-message', '<strong>' . $model->name . '</strong> has been saved.');

            if (Yii::$app->request->post('save_more')) {
                return $this->redirect(['create']);
            }

            return $this->redirect(['view', <?= $urlParams ?>]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing <?= $modelClass ?> model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * <?= implode("\n     * ", $actionParamComments) . "\n" ?>
     * @return mixed
     */
    public function actionUpdate(<?= $actionParams ?>)
    {
        // if (!Yii::$app->user->can('update<?= $modelClass ?>')) {
        //     throw new ForbiddenHttpException("You are not allowed to perform this action.");
        // }

        $model = $this->findModel(<?= $actionParams ?>);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('admin-flash-level', 'success');
            Yii::$app->session->setFlash('admin-flash-message', '<strong>' . $model->name . '</strong> has been saved.');

            if (Yii::$app->request->post('save_more')) {
                return $this->redirect(['create']);
            }

            return $this->redirect(['view', <?= $urlParams ?>]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing <?= $modelClass ?> model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * <?= implode("\n     * ", $actionParamComments) . "\n" ?>
     * @return mixed
     */
    public function actionDelete(<?= $actionParams ?>)
    {
        // if (!Yii::$app->user->can('delete<?= $modelClass ?>')) {
        //     throw new ForbiddenHttpException("You are not allowed to perform this action.");
        // }

        $model = $this->findModel(<?= $actionParams ?>);

        if ($model->delete()) {
            Yii::$app->session->setFlash('admin-flash-level', 'success');
            Yii::$app->session->setFlash('admin-flash-message', '<strong>' . isset($model->name) ? $model->name : 'Data' . '</strong> has successfully been removed.');
        } else {
            Yii::$app->session->setFlash('admin-flash-level', 'danger');
            Yii::$app->session->setFlash('admin-flash-message', 'Failed to remove <strong>' . isset($model->name) ? $model->name : 'data' . '</strong>');
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the <?= $modelClass ?> model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * <?= implode("\n     * ", $actionParamComments) . "\n" ?>
     * @return <?=                   $modelClass ?> the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(<?= $actionParams ?>)
    {
<?php
if (count($pks) === 1) {
    $condition = '$id';
} else {
    $condition = [];
    foreach ($pks as $pk) {
        $condition[] = "'$pk' => \$$pk";
    }
    $condition = '[' . implode(', ', $condition) . ']';
}
?>
        if (($model = <?= $modelClass ?>::findOne(<?= $condition ?>)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
