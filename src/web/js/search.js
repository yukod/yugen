$(document).ready(function() {
    function filter(el) {
        var q = $(el).val(),
            url = window.location.protocol + "//" + window.location.host + window.location.pathname + '?' + searchModel+ '[title]=' + q;
        console.log(url);
        window.location = url;
    }

    $('.search-field').on('change', function() {
        filter(this);
    })

    $('#search-form').submit(function(e) {
        e.preventDefault();
        filter('.search-field');
    });
})
