<?php

namespace yukod\yugen\assets;

use yii\web\AssetBundle;

/**
 * Asset bundle for Yu Generator.
 *
 * @author Nur Wahyudin <wahyu1790@gmail.com>
 */
class YugenAsset extends AssetBundle
{
    public $sourcePath = '@vendor/yukod/yugen/src/web';
    public $baseUrl = '@web';
    public $css = [];
    public $js = [
        'js/search.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
    public $publishOptions = [
        'forceCopy' => YII_DEBUG,
    ];
}