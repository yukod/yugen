

```
    $config['modules']['gii'] = [
        'generators' => [
            'crud' => [
                'class' => 'yii\gii\generators\crud\Generator',
                'templates' => [
                    'AdminLTE' => '@common/generators/crud/AdminLTE',
                ]
            ],
            'model' => [
                'class' => 'yii\gii\generators\model\Generator',
                'templates' => [
                    'default' => '@common/generators/model/default',
                ]
            ],
            ...
        ],
    ];
```